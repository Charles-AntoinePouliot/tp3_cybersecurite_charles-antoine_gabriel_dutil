from flask import Flask, json, request, jsonify, make_response
import hashlib
import uuid
import datetime
import sys
import jwt
import dbHandler
import secrets

ACCESS_DENIED = "Access denied."
AUTHENTICATION_NEEDED = "You need to be authenticated to perform this action"

app = Flask("TRACK2APP")

app.config["SECRET_KEY"] = "something super secret"

def generate_password_hash(password):
    hash = hashlib.md5(password.encode())
    print(hash.hexdigest(), flush=True)
    return hash.hexdigest()


def check_password_hash(userPassword, password):
    print(password)
    print(userPassword)
    hash = hashlib.md5(password.encode()).hexdigest()
    if hash == userPassword:
        return True
    return False


def getInitialClientIP(request):
    print("trying to get IP", flush=True)
    ip = None

    if request.data:
        if request.is_json:
            data = request.get_json()
            if "ip" in data:
                ip = data["ip"]
        else:
            print("not in json", flush=True)

    if (
        ip == None
    ):  # No Ip found in the request json body, so the request should come directly from client.
        ip = request.remote_addr
    return ip


def get_user_from_token(request):
    token = None
    if "x-access-token" in request.headers:
        token = request.headers["x-access-token"]

    if not token:
        raise ValueError("Token is missing.")

    try:
        data = jwt.decode(token, app.config["SECRET_KEY"])
        user_id = data["user_id"]

        client_ip = getInitialClientIP(request)
        token_ip = data["ip"]
        if token_ip == None:
            raise ValueError("Token is not ip bound, old tken now invalid.")
        elif not client_ip == token_ip:
            raise ValueError(
                "Token is for an IP ("
                + token_ip
                + ") different than the client ("
                + client_ip
                + "). Not valid."
            )
        jwt_hash = hashlib.md5(token.encode()).hexdigest()
        token_session = dbHandler.get_session_token(jwt_hash)
        # token_session = TokenSess.query.filter_by(id=jwt_hash).first()
        if token_session == None:
            raise ValueError("Token has been revoked server side.")

        return dbHandler.get_user(user_id)
        # return User.query.filter_by(id=user_id).first()
    except:
        raise ValueError("Token is invalid")


@app.route("/csrftoken", methods=["GET"])
def getCSRFToken():
    # We validate the jwt token
    try:
        get_user_from_token(request)
    except Exception as e:
        print("Problem with JWT token validation: " + str(e), flush=True)
        return jsonify({"message": "Problem with JWT token validation."})

    token = request.headers["x-access-token"]
    jwt_hash = hashlib.md5(token.encode()).hexdigest()
    token_session = dbHandler.get_session_token(jwt_hash)
    # token_session = TokenSess.query.filter_by(id=jwt_hash).first()

    return jsonify({"message": "CSRF Token Retrieval", "csrfToken": token_session.part})


@app.route("/csrfstatus", methods=["GET"])
def validateCSRFToken():
    try:
        get_user_from_token(request)
    except Exception as e:
        print("Problem with JWT token validation: " + str(e), flush=True)
        return jsonify({"message": "Problem with JWT token validation."})

    token = request.headers["x-access-token"]

    data = request.get_json()
    if not "csrfToken" in data:
        print("CSRF validation issue: no csrf token provided.", flush=True)
        return jsonify({"message": "CSRF Token validation problem."})

    csrf_token = data["csrfToken"]
    jwt_hash = hashlib.md5(token.encode()).hexdigest()
    token_session = dbHandler.get_session_token(jwt_hash)
    # token_session = TokenSess.query.filter_by(id=jwt_hash).first()
    if token_session.part == csrf_token:
        print("CSRF token validation success.", flush=True)
        return jsonify(
            {"message": "CSRF token validation success.", "correct": csrf_token}
        )

    print("provided csrf token: " + csrf_token, flush=True)
    print("csrf token associated with jwt: " + token_session.part, flush=True)

    print(
        "CSRF validation issue: csrf token provided does not match jwt token.",
        flush=True,
    )
    return jsonify({"message": "CSRF Token validation problem."})


@app.route("/")
def hello():
    rep = dict()
    rep["message"] = "Welcome to the Gaube Bank application"
    ip = getInitialClientIP(request)
    rep["ip"] = ip

    token = None
    if "x-access-token" in request.headers:
        token = request.headers["x-access-token"]

    if not token:
        rep["user"] = "not authenticated"
    else:
        rep["user"] = get_user_from_token(request).username

    return rep

@app.route("/account/<p_id>", methods=["GET"])
def get_account_info(p_id):
    #only admin but not made
    actions = dbHandler.get_actions_for_account(p_id)
    return jsonify({"actions": actions})

@app.route("/createuser", methods=["POST"])
def create_user():
    # Should only be available to admin
    try:
        current_user = get_user_from_token(request)
    except Exception as e:
        print(str(e), flush=True)
        return jsonify({"message": ACCESS_DENIED}), 401

    if current_user.admin:
        data = request.get_json()
        hashed_password = generate_password_hash(data["password"])
        username = data["username"]
        dbHandler.create_user(uuid.uuid4(), username, hashed_password)
        return jsonify({"message": "new user created"})

    return jsonify({"message": ACCESS_DENIED})

@app.route("/deposit", methods=["POST"])
def deposit_in_account():
    data = request.get_json()
    id = uuid.uuid4()
    dbHandler.create_action(
        str(id), data["text"], "Deposit", "rando"
    )
    return jsonify({"message": "Money deposited for rando"})


@app.route("/withdraw", methods=["POST"])
def withdraw_from_account():
    data = request.get_json()
    id = uuid.uuid4()
    dbHandler.create_action(
        str(id), data["text"], "Withdraw", "randomName"
    )
    return jsonify({"message": "Money withdrawn for " + "randomName"})


@app.route("/login", methods=["post"])
def login():
    print("LOGIN route", flush=True)
    ip = getInitialClientIP(request)
    print("LOGIN attempt from : " + ip, flush=True)

    auth = request.authorization
    if not auth or not auth.username or not auth.password:
        print("LOGIN: problem with request.authorization", flush=True)
        return jsonify({"message": "Could not authenticate you."}), 401

    user = dbHandler.get_user(auth.username)
    # user = User.query.filter_by(name=auth.username).first()
    if not user:
        print("LOGIN: No such user", flush=True)
        return jsonify({"message": "Could not authenticate you."}), 401

    if check_password_hash(user["password"], auth.password):
        token = jwt.encode(
            {
                'user_id': user["id"],
                'ip': ip,
            },
            app.config["SECRET_KEY"]
        ).encode("utf-8")
        jwt_hash = hashlib.md5(token).hexdigest()
        part2 = secrets.token_urlsafe(64)
        dbHandler.save_session_token(jwt_hash, part2)

        return jsonify(
            {
                "message": "Your are successfully authenticated"
                + ip
                + ". Welcome: "
                + user["name"],
                "token": token.decode("utf-8"),
            }
        )

    print("LOGIN: Wrong password", flush=True)
    return jsonify({"message": "Could not authenticate you."}), 401


@app.route("/logout", methods=["post"])
def logout():
    if "x-access-token" in request.headers:
        token = request.headers["x-access-token"]
        jwt_hash = hashlib.md5(token.encode()).hexdigest()
        token_session = dbHandler.get_session_token(jwt_hash)
        # token_session = TokenSess.query.filter_by(id=jwt_hash).first()
        if not token_session:
            return jsonify({"message": "No corresponding token active found."})
        dbHandler.delete_session_token(jwt_hash)
        # db.session.delete(token_session)
        # db.session.commit()

        return jsonify({"message": "token revoked, you are now logged out."})

    return jsonify({"message": "No x-access-token, you were not logged in."})

@app.route("/flag", methods=["GET"])
def getSecretFlag():
    try:
        current_user = get_user_from_token(request)
    except Exception as e:
        print(str(e), flush=True)
        return jsonify({"message": ACCESS_DENIED}), 401

    if current_user.admin:
        return jsonify({"message": "Hello, the secret flag is FLAG-8241"})

    return jsonify({"message": "Sorry, only admin can see the flag"})

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5555)
