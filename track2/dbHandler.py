import sqlite3


def create_connection():
    db_file = "./track2/accounts.db"
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Exception as e:
        print(e)
    return conn


def save_session_token(jwt_hash, part):
    conn = create_connection()
    cur = conn.cursor()
    cur.executescript(
        "INSERT INTO token (id, part) VALUES('" + jwt_hash + "', '" + part + "');"
    )

    conn.commit()
    conn.close()


def delete_session_token(jwt_hash):
    conn = create_connection()
    cur = conn.cursor()
    cur.execute("DELETE FROM token WHERE jwt_hash = " + jwt_hash)

    conn.commit()
    conn.close()


def get_session_token(jwt_hash):
    conn = create_connection()
    cur = conn.cursor()
    cur.execute("SELECT * FROM token WHERE jwt_hash = " + jwt_hash)

    row = cur.fetchall()[0]
    print("ROW:" + str(row))
    token_data = {}
    token_data["jwt_hash"] = row[0]
    token_data["part"] = row[1]
    conn.close()
    return token_data


def get_actions_for_account(user_id):
    conn = create_connection()
    cur = conn.cursor()
    cur.execute("SELECT * FROM action WHERE user_id = " + user_id)

    rows = cur.fetchall()
    actions = []
    for row in rows:
        action_data = {}
        action_data["user_id"] = row[0]
        action_data["amount"] = row[1]
        action_data["type"] = row[2]
        action_data["name"] = row[3]
        actions.append(action_data)
    conn.close()
    return actions


def create_action(user_id, amount, type, name):
    conn = create_connection()
    cur = conn.cursor()
    cur.executescript(
        "INSERT INTO action (user_id, amount, type, name) VALUES('"
        + str(user_id)
        + "', '"
        + amount
        + "', '"
        + type
        + "', '"
        + name
        + "');"
    )
    conn.commit()
    conn.close()


def create_user(uuid, name, password):
    conn = create_connection()
    cur = conn.cursor()
    cur.executescript(
        "INSERT INTO user (id, name, password, admin) VALUES('"
        + uuid
        + "', '"
        + name
        + "', '"
        + password
        + "', False);"
    )
    conn.commit()
    conn.close()


def create_account(uuid, amount, type, name):
    conn = create_connection()
    cur = conn.cursor()
    cur.executescript(
        "INSERT INTO account (user_id, amount, type, name) VALUES('"
        + uuid
        + "', '"
        + amount
        + "', '"
        + type
        + "', '"
        + name
        + "');"
    )
    conn.commit()
    conn.close()


def get_all_accounts():
    conn = create_connection()
    cur = conn.cursor()
    cur.execute("SELECT * FROM account")

    rows = cur.fetchall()
    accounts = []
    for row in rows:
        account_data = {}
        account_data["user_id"] = row[0]
        account_data["amount"] = row[1]
        account_data["type"] = row[2]
        account_data["name"] = row[3]
        accounts.append(account_data)
    conn.close()
    return accounts


def create_action(user_id, amount, type, name):
    conn = create_connection()
    cur = conn.cursor()
    cur.executescript(
        "INSERT INTO action (user_id, amount, type, name) VALUES('"
        + user_id
        + "', '"
        + amount
        + "', '"
        + type
        + "', '"
        + name
        + "');"
    )
    conn.commit()
    conn.close()


def get_user(name):
    conn = create_connection()
    cur = conn.cursor()
    cur.execute("SELECT * FROM user WHERE name = '" + str(name) + "'")

    row = cur.fetchall()[0]
    print("ROW:" + str(row))
    user_data = {}
    user_data["id"] = row[0]
    user_data["name"] = row[1]
    user_data["password"] = row[2]
    user_data["admin"] = row[3]
    conn.close()
    return user_data
