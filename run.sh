#!/bin/bash

# Copy templates from both tracks
rm -dr templates
mkdir templates
cp ./track1/templates/track1.html ./templates/
cp ./track2/templates/track2.html ./templates/

# Stops and restarts all services
docker-compose -f ./docker-compose.yml --project-directory . down
docker-compose -f ./docker-compose.yml --project-directory . build
docker-compose -f ./docker-compose.yml --project-directory . up -d

echo "Track 1 back end running on port 5550"
echo "Track 1 front end running on port 5551"
echo "Track 2 front end running on port 5552"
echo "Track 2 back end running on port 5555"