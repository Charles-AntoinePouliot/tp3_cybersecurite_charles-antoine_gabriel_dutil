import sqlite3


def create_connection():
    db_file = "./track1/accounts.db"
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except error as e:
        print(e)
    return conn


def save_session_token(jwt_hash, part):
    conn = create_connection()
    cur = conn.cursor()
    cur.executescript(
        "INSERT INTO token (id, part) VALUES('" + jwt_hash + "', '" + part + "');"
    )

    conn.commit()
    conn.close()


def delete_session_token(jwt_hash):
    conn = create_connection()
    cur = conn.cursor()
    query = "DELETE FROM token WHERE id = '" + str(jwt_hash) + "';"
    cur.execute(query)

    conn.commit()
    conn.close()


def get_session_token(jwt_hash):
    conn = create_connection()
    cur = conn.cursor()
    query = "SELECT * FROM token WHERE id = '" + str(jwt_hash) + "';"
    cur.execute(query)

    row = cur.fetchall()[0]
    print("ROW:" + str(row))
    token_data = {}
    token_data["jwt_hash"] = row[0]
    token_data["part"] = row[1]
    conn.close()
    return token_data


def get_actions_for_account(user_id):
    conn = create_connection()
    cur = conn.cursor()
    query = "SELECT * FROM action WHERE user_id = '" + str(user_id) + "';"
    cur.execute(query)

    rows = cur.fetchall()
    actions = []
    for row in rows:
        action_data = {}
        action_data["id"] = row[0]
        action_data["user_id"] = row[1]
        action_data["amount"] = row[2]
        action_data["type"] = row[3]
        actions.append(action_data)
    conn.close()
    return actions


# SQL INJECTION ICI
def get_single_action_for_account(user_id, action_id):
    conn = create_connection()
    cur = conn.cursor()
    query = (
        "SELECT * FROM action WHERE user_id = '"
        + str(user_id)
        + "' AND id = "
        + action_id
        + ";"
    )
    cur.execute(query)

    rows = cur.fetchall()
    actions = []
    for row in rows:
        action_data = {}
        action_data["id"] = row[0]
        action_data["user_id"] = row[1]
        action_data["amount"] = row[2]
        action_data["type"] = row[3]
        actions.append(action_data)
    conn.close()
    return actions


def create_action(user_id, amount, type):
    conn = create_connection()
    cur = conn.cursor()
    cur.executescript(
        f'INSERT INTO action (user_id, amount, type) VALUES ({user_id}, "{amount}", "{type}");'
    )
    conn.commit()
    conn.close()


# INSERTION SCRIPT XSS ICI
def create_user(name, password):
    decodedPassword = password.decode("utf-8")  # Decoded, but stil hashed
    conn = create_connection()
    cur = conn.cursor()
    query = (
        "INSERT INTO main.user (name, password, admin) VALUES('"
        + name
        + "', '"
        + decodedPassword
        + "', False);"
    )
    print(query, flush=True)
    cur.executescript(query)
    conn.commit()
    conn.close()


def get_user_by_name(name):
    conn = create_connection()
    cur = conn.cursor()
    query = "SELECT * FROM user WHERE name = '" + str(name) + "';"
    cur.execute(query)

    row = cur.fetchall()[0]
    user_data = {}
    user_data["id"] = row[0]
    user_data["name"] = row[1]
    user_data["password"] = row[2]
    user_data["admin"] = row[3]
    conn.close()
    return user_data


def get_user_by_id(id):
    conn = create_connection()
    cur = conn.cursor()
    query = "SELECT * FROM user WHERE id = '" + str(id) + "';"
    cur.execute(query)

    row = cur.fetchall()[0]
    user_data = {}
    user_data["id"] = row[0]
    user_data["name"] = row[1]
    user_data["password"] = row[2]
    user_data["admin"] = row[3]
    conn.close()
    return user_data


# RECUPERATION SCRIPT XSS ICI
def get_all_users():
    conn = create_connection()
    cur = conn.cursor()
    query = "SELECT * FROM user;"
    cur.execute(query)

    rows = cur.fetchall()
    users = []
    for row in rows:
        user_data = {}
        user_data["id"] = row[0]
        user_data["name"] = row[1]
        user_data["password"] = row[2]
        user_data["admin"] = row[3]
        users.append(user_data)
    conn.close()
    print(users, flush=True)
    return users
