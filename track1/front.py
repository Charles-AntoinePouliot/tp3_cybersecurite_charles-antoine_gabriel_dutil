from flask import Flask, request, jsonify, make_response, render_template
import requests
import json

app = Flask("FrontEnd")

BACK_END_IP = "track_1_api"
BACK_END_PORT = "5550"


def build_home_page(messages):
    ip = request.remote_addr

    token = request.cookies.get("jwt")
    response = requests.get(
        "http://" + BACK_END_IP + ":" + BACK_END_PORT + "/",
        json={"ip": ip},
        headers={"x-access-token": token},
    )
    data = None
    if response.status_code == 200:
        data = json.loads(response.content.decode("utf-8"))

    else:
        data = {"message": "ERROR"}

    content_basic = str(data)

    content_action = str(messages)

    return render_template(
        "track1.html",
        content_basic=content_basic,
        content_action=content_action,
    )


def build_response(response):
    if response.status_code == 200:
        obj = json.loads(response.content.decode("utf-8"))
        resp = make_response(build_home_page(obj))
        return resp
    else:
        msg = response.content.decode("utf-8")
        resp = make_response(build_home_page({"message": msg}))
        return resp


@app.route("/")
def hello():
    return build_home_page({})


@app.route("/deposit", methods=["POST"])
def deposit():
    data = {"text": request.form["deposit"], "ip": request.remote_addr}
    token = request.cookies.get("jwt")
    resp = requests.post(
        "http://" + BACK_END_IP + ":" + BACK_END_PORT + "/deposit",
        json=data,
        headers={"x-access-token": token},
    )
    return build_response(resp)


@app.route("/withdraw", methods=["POST"])
def withdraw():
    data = {"text": request.form["withdraw"], "ip": request.remote_addr}
    token = request.cookies.get("jwt")

    resp = requests.post(
        "http://" + BACK_END_IP + ":" + BACK_END_PORT + "/withdraw",
        json=data,
        headers={"x-access-token": token},
    )
    return build_response(resp)


@app.route("/createuser", methods=["POST"])
def createuser():
    data = {
        "username": request.form["username"],
        "password": request.form["password"],
        "ip": request.remote_addr,
    }
    token = request.cookies.get("jwt")

    resp = requests.post(
        "http://" + BACK_END_IP + ":" + BACK_END_PORT + "/createuser",
        json=data,
        headers={"x-access-token": token},
    )
    return build_response(resp)


@app.route("/account", methods=["GET"])
def viewAccount():
    token = request.cookies.get("jwt")

    data = {"ip": request.remote_addr}
    response = requests.get(
        "http://" + BACK_END_IP + ":" + BACK_END_PORT + "/account",
        json=data,
        headers={"x-access-token": token},
    )
    return build_response(response)


@app.route("/action", methods=["POST"])
def viewSingleTransactionForAccount():
    data = {"text": request.form["action"], "ip": request.remote_addr}
    token = request.cookies.get("jwt")
    response = requests.post(
        "http://" + BACK_END_IP + ":" + BACK_END_PORT + "/action",
        json=data,
        headers={"x-access-token": token},
    )
    return build_response(response)


@app.route("/users", methods=["GET"])
def viewAllUsers():
    token = request.cookies.get("jwt")

    data = {"ip": request.remote_addr}
    response = requests.get(
        "http://" + BACK_END_IP + ":" + BACK_END_PORT + "/users",
        json=data,
        headers={"x-access-token": token},
    )
    return build_response(response)


@app.route("/login", methods=["POST"])
def login():
    ip = request.remote_addr
    data = {"ip": ip}
    response = requests.post(
        "http://" + BACK_END_IP + ":" + BACK_END_PORT + "/login",
        auth=(request.form["username"], request.form["password"]),
        json=data,
    )
    resp = build_response(response)

    if response.status_code == 200:

        obj = json.loads(response.content.decode("utf-8"))
        resp.set_cookie("jwt", obj["token"])  # FIX METTRE HTTPONLY A TRUE

    return resp


@app.route("/logout", methods=["POST"])
def logout():
    token = request.cookies.get("jwt")
    data = {"ip": request.remote_addr}
    response = requests.post(
        "http://" + BACK_END_IP + ":" + BACK_END_PORT + "/logout",
        json=data,
        headers={"x-access-token": token},
    )
    resp = build_response(response)
    resp.set_cookie("jwt", "dummy", expires=0)
    return resp


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5551)
