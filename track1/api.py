from flask import Flask, request, jsonify, make_response
from flask_bcrypt import Bcrypt
import datetime
import sys
import jwt

# import bleach
import hashlib
import secrets
import dbHandler

ACCESS_DENIED = "Access denied."
AUTHENTICATION_NEEDED = "You need to be authenticated to perform this action"

app = Flask("BANK APP")

app.config["SECRET_KEY"] = "something super secret"

app.config["BCRYPT_HANDLE_LONG_PASSWORDS"] = True
flask_bcrypt = Bcrypt(app)


def getInitialClientIP(request):
    print("trying to get IP", flush=True)
    ip = None

    if request.data:
        if request.is_json:
            data = request.get_json()
            if "ip" in data:
                ip = data["ip"]
        else:
            print("not in json", flush=True)

    if (
        ip == None
    ):  # No Ip found in the request json body, so the request should come directly from client.
        ip = request.remote_addr
    return ip


def get_user_from_token(request):
    token = None
    if "x-access-token" in request.headers:
        token = request.headers["x-access-token"]

    if not token:
        raise ValueError("Token is missing.")

    try:
        data = jwt.decode(
            token,
            app.config["SECRET_KEY"],
            algorithms=["HS256"],
        )
        user_id = data["user_id"]

        client_ip = getInitialClientIP(request)
        token_ip = data["ip"]
        if token_ip == None:
            raise ValueError("Token is not ip bound, old tken now invalid.")
        elif not client_ip == token_ip:
            raise ValueError(
                "Token is for an IP ("
                + token_ip
                + ") different than the client ("
                + client_ip
                + "). Not valid."
            )
        jwt_hash = hashlib.md5(token.encode("utf-8")).hexdigest()
        token_session = dbHandler.get_session_token(jwt_hash)
        if token_session == None:
            raise ValueError("Token has been revoked server side.")

        return dbHandler.get_user_by_id(user_id)
    except:
        raise ValueError("Token is invalid")


@app.route("/")
def hello():
    rep = dict()
    rep["message"] = "Welcome to the Gaube Bank application"
    ip = getInitialClientIP(request)
    rep["ip"] = ip

    token = None
    if "x-access-token" in request.headers:
        token = request.headers["x-access-token"]

    if not token:
        rep["user"] = "not authenticated"
    else:
        user = get_user_from_token(request)
        print(user, flush=True)
        rep["user"] = user["name"]

    return rep


@app.route("/createuser", methods=["POST"])
def create_user():
    data = request.get_json()
    hashed_password = flask_bcrypt.generate_password_hash(data["password"], 13)
    username = data["username"]
    dbHandler.create_user(username, hashed_password)
    return jsonify({"message": "new user created"})


@app.route("/users", methods=["GET"])
def view_all_users():
    try:
        current_user = get_user_from_token(request)
    except Exception as e:
        print(str(e), flush=True)
        return jsonify({"message": AUTHENTICATION_NEEDED}), 401

    if not current_user["admin"]:
        return jsonify({"message": "You need to be an admin to perform this action"})
    users = dbHandler.get_all_users()
    print(users, flush=True)
    return jsonify({"users": users})


@app.route("/account", methods=["GET"])
def get_account_info():
    try:
        current_user = get_user_from_token(request)
    except Exception as e:
        print(str(e), flush=True)
        return jsonify({"message": AUTHENTICATION_NEEDED}), 401

    if not current_user["admin"]:
        actions = dbHandler.get_actions_for_account(current_user["id"])
        return jsonify({"actions": actions})
    if current_user["admin"]:
        # ÉPREUVE C) - get_account_info en tant qu'admin
        return jsonify({"Flag-": "0041"})


@app.route("/action", methods=["POST"])
def view_single_transaction():
    # Should only be available to users (not admins)
    try:
        current_user = get_user_from_token(request)
    except Exception as e:
        print(str(e), flush=True)
        return jsonify({"message": AUTHENTICATION_NEEDED}), 401

    if not current_user["admin"]:
        data = request.get_json()
        try:
            action = dbHandler.get_single_action_for_account(
                current_user["id"], data["text"]
            )
            print(action, flush=True)
        except:
            return jsonify({"message": "This transaction doesn't exist."}), 401
        return jsonify({"action": action})
    return jsonify(
        {
            "message": "Cannot view single transactions for an admin account. Try using view all transactions."
        }
    )


@app.route("/deposit", methods=["POST"])
def deposit_in_account():
    # Should only be available to users (not admins)

    try:
        current_user = get_user_from_token(request)
    except Exception as e:
        print(str(e), flush=True)
        return jsonify({"message": AUTHENTICATION_NEEDED}), 401

    if not current_user["admin"]:
        data = request.get_json()
        dbHandler.create_action(current_user["id"], data["text"], "Deposit")
        return jsonify(
            {"message": data["text"] + "$ deposited for " + current_user["name"]}
        )
    return jsonify({"message": "Cannot deposit money for an admin account"})


@app.route("/withdraw", methods=["POST"])
def withdraw_from_account():
    # Should only be available to users (not admins)

    try:
        current_user = get_user_from_token(request)
    except Exception as e:
        print(str(e), flush=True)
        return jsonify({"message": AUTHENTICATION_NEEDED}), 401

    if not current_user["admin"]:
        data = request.get_json()
        dbHandler.create_action(current_user["id"], data["text"], "Withdraw")
        return jsonify(
            {"message": data["text"] + "$ withdrawn for " + current_user["name"]}
        )
    return jsonify({"message": "Cannot withdraw money from an admin account"})


@app.route("/login", methods=["post"])
def login():
    print("LOGIN route", flush=True)
    ip = getInitialClientIP(request)
    print("LOGIN attempt from : " + ip, flush=True)

    auth = request.authorization
    if not auth or not auth.username or not auth.password:
        print("LOGIN: problem with request.authorization", flush=True)
        return jsonify({"message": "Could not authenticate you."}), 401

    try:
        user = dbHandler.get_user_by_name(auth.username)
    except:
        print("LOGIN: No such user", flush=True)
        return jsonify({"message": "Could not authenticate you."}), 401

    if not user:
        print("LOGIN: No such user", flush=True)
        return jsonify({"message": "Could not authenticate you."}), 401

    if flask_bcrypt.check_password_hash(user["password"], auth.password):
        if not user["admin"]:
            token = jwt.encode(
                {
                    "user_id": user["id"],
                    "ip": ip,
                    # "exp": datetime.datetime.utcnow() + datetime.timedelta(minutes=10),
                },
                app.config["SECRET_KEY"],
            ).encode("utf-8")
            jwt_hash = hashlib.md5(token).hexdigest()
        else:
            token = jwt.encode(
                {
                    "user_id": user["id"],
                    "ip": ip,
                    "flag": "2657",
                    # "exp": datetime.datetime.utcnow() + datetime.timedelta(minutes=10),
                },
                app.config["SECRET_KEY"],
            ).encode("utf-8")
            jwt_hash = hashlib.md5(token).hexdigest()
        part2 = secrets.token_urlsafe(64)
        dbHandler.save_session_token(jwt_hash, part2)

        return jsonify(
            {
                "message": "Your are successfully authenticated from "
                + ip
                + ". Welcome: "
                + user["name"],
                "token": token.decode("utf-8"),
            }
        )

    print("LOGIN: Wrong password", flush=True)
    return jsonify({"message": "Could not authenticate you."}), 401


@app.route("/logout", methods=["post"])
def logout():
    if "x-access-token" in request.headers:
        token = request.headers["x-access-token"]
        jwt_hash = hashlib.md5(token.encode("utf-8")).hexdigest()
        token_session = dbHandler.get_session_token(jwt_hash)

        if not token_session:
            return jsonify({"message": "No corresponding token active found."})
        dbHandler.delete_session_token(jwt_hash)

        return jsonify({"message": "token revoked, you are now logged out."})

    return jsonify({"message": "No x-access-token, you were not logged in."})


if __name__ == "__main__":
    # db.create_all()
    app.run(debug=True, host="0.0.0.0", port=5550)
