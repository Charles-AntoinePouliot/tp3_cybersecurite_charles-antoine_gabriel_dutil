const puppeteer = require('puppeteer');

(async () => {
  console.log("start")
  const browser = await puppeteer.launch({userDataDir: "./user_data", headless:true, args:['--no-sandbox']}); //user_data is a file in which the browser will store persisting info such as cookie and local storage
  console.log("loading main page")
  const loginPage = await browser.newPage();
  await loginPage.goto('http:track_1_front:5551');
  await loginPage.waitForTimeout(5000)
  await loginPage.type('#username','Admin');
  await loginPage.type('#password','12345');
  await loginPage.click('#login')
  await loginPage.waitForNavigation();

  const page = await browser.newPage(); 
  const response = await page.goto('http:track_1_front:5551/users');
  await page.waitForTimeout(5000) 
  console.log(await response.text())
  await browser.close();
})();
